#include<iostream>
#define pi 3.14
using namespace std;
/*descripcion de la funcion:
Esta es la funcion main de nuestro programa,
es la clase de taller de sw
dirigida por:Jorge Salgado y Luis Arellano*/

int main(){
	//declaracion de variables
	float radioInput;
	float AlturaInput;
	float AreaLateralresult;
	float Volumenresult;
	
	//inicializacion
	radioInput= 0 ;
	AlturaInput = 0 ;
	AreaLateralresult = 0 ;
	Volumenresult = 0 ;
	//visualizacion de la variable
	
	cout<<"ingresa el radio: ";cin>>radioInput;
	cout<<"ingresa la altura: ";cin>>AlturaInput;
	
	AreaLateralresult = 2*pi*radioInput*AlturaInput;
	Volumenresult = radioInput*radioInput*AlturaInput*pi;
	
	cout<<"El Area Lateral del Cilindro es: "<<AreaLateralresult<<"\r\n";
	cout<<"El Volumen del Cilindro es : "<<Volumenresult<<"\r\n";
	
	return  0;
	

	
}
